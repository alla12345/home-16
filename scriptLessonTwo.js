//1--------
const age = 2023;
if (age > 2021) {
    console.log('Задание 2.1','вас еще нету');
} else if (age > 2010 && age < 2021) {
    console.log('Задание 2.1','вы зумер');
} else {
    console.log('Задание 2.1','вы бумер');
}


//2----------
switch (age) {
  case 2021: {
    console.log('Задание 2.2','день добрый');
    break;
  }
  case 2020: {
    console.log('Задание 2.2','день добрый из прошлого');
    break;
  }
  case 2022: {
    console.log('Задание 2.2','день добрый из будущего');
    break;
  }
  default: {
    console.log('Задание 2.2','привет');
  }
}

//3-----------
let str = 'a: 123 tth';
str = str.toUpperCase();
let newStr = '';

for (let i = 0; i < str.length; i++) {
  if (isNaN(Number(str[i])) || str[i] === ' ')  {
    newStr = newStr + str[i];
       
  } else {
    let num = Number(str[i]);
    newStr = newStr + (num + i);      
  } 
}
str = newStr;

//4------------
let counter = 0; 
while (counter !== 2) {
  let number = Math.floor(Math.random() * (4));
  if (number === 3) {
    counter = counter + 1;
  } else { 
    counter = 0;
  }
  console.log('Задание 2.4', number);
}

 
