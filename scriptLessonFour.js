//1---
let name = 'Masha';
function sayHiOrBy(isHi) {
    isHi ? console.log('Задание 4.1',`hi ${name}`) :  console.log('Задание 4.1',`by ${name}`);
}
sayHiOrBy(false);

//2---
function sayHiOrByWithName(isHi, name) {
    isHi ? console.log('Задание 4.2',`hi ${name}`) :  console.log('Задание 4.2',`by ${name}`);
}
sayHiOrByWithName(true, 'Dima');

//3------------------------------
const arrN = [40, 5, 20, 8 ,7];

function getMinArrN(arr, x) {
    let minArrN = arrN[0];
    for (key in arrN) {
        if(arrN[key] > x) {
            minArrN = undefined;
            break;  
        } else { 
            if(minArrN >= arrN[key]) {
               minArrN = arrN[key];
            }
        }
    }
    return minArrN;
}

const minValue = getMinArrN(arrN, 102);

//4-------------
const arr = [5, 7, 7, 5, 2, 7];

function getMaxFromArr(arr, fun) {
    let maxFromArr = arr[0];
    for (key in arr) {
        if(maxFromArr < arr[key]) {
            maxFromArr = arr[key];  
        }   
    }
    fun(maxFromArr);
}

function showMaxValue(maxValue) {
    console.log(`Задание 4.4,  max элемент ${maxValue}`);
}

function  compareWithMaxValue(maxValue) {
    maxValue === 7 ? console.log('Задание 4.4', 'max элемент  равен 7')
     : console.log('Задание 4.4',' max элемент не равен 7');
}

getMaxFromArr(arr, showMaxValue);
getMaxFromArr(arr, compareWithMaxValue);

const arr2 = [25, 40, 22, 17];
getMaxFromArr(arr2, showMaxValue);
getMaxFromArr(arr2, compareWithMaxValue);

const arr3 = [1, 2, 3];
getMaxFromArr(arr3, showMaxValue);
getMaxFromArr(arr3, compareWithMaxValue);

//5--------
let people = [
    {name: 'Masha', age: 26},
    {name: 'Dasha', age: 17},
    {name: 'Sasha', age: 42},
    {name: 'Dima', age: 15},
    {name: 'Fima', age: 34},
]
// function sortAge(arr) {
//     arr.sort((a, b) => a.age > b.age ? 1 : -1);
// }
// sortAge(people);
people.sort((a, b) => a.age > b.age ? 1 : -1);

people = people.filter(person => person.age >= 18);

const strNameArr = people.reduce((total, person) => total + person.name, '');
console.log('Задание 4.5', strNameArr);
// console.log(people.reduce ((total, person) => total + person.name, ''));

people.forEach((item, index) => {
    console.log('Задание 4.5', `hi ${item.name}`);
    item.age = item.age + index;
});

