//1 и 2 ---
const obj = {
  name: 'Masha',
  lastName: 'Petrova',
  age: 20,
  son: {
    name: 'dima',
    age: 1,
  }
}

let sum = '';
let keyOne = '';
for (let key in obj) {
  if (typeof obj[key] === 'string') {
    if(keyOne === '') {
      keyOne = obj[key];          
    }
    sum = sum + obj[key];
    delete obj[key];
  }
}
obj[keyOne] = sum;

//3---
const cop = {...obj};
//3.1 не надежно, т.к. не знаю что в поле (undefined)
//console.log(cop.aav ? 'есть' : 'нет');
console.log('Задание 3.3','aav' in cop ? 'есть aav' : 'нет aav');

//4---
const arra = [21, 15, 65];
const arrb = [3, 3, 52];
const arrc = [arra[0] + arrb[0], ...arra, ...arrb, arra[arra.length - 1] + arrb[arrb.length - 1]];

//5---
let minIndex = 0;
let maxIndex = 0;

for (key in arrc) {
  if (arrc[minIndex] > arrc[key]) {
    minIndex = key;
  }
  if (arrc[maxIndex] < arrc[key]) {
    maxIndex = key;
  }
}

const rememberMin = arrc[minIndex];
arrc[minIndex] = arrc[maxIndex];
arrc[maxIndex] = rememberMin;
